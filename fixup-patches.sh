#!/bin/sh
set -e
origdir=$(pwd)
tempdir=$(mktemp -d fixup_$1_XXXXXX)
cd $tempdir
git clone git@salsa.debian.org:emacsen-team/$1
cd $1
test $(dpkg-parsechangelog -S Distribution) = 'unstable'
grep -q 'Maintainer: Debian Emacs' debian/control || (echo not a team package? && /bin/false)
(test -d debian/patches && test -n "$(find debian/patches -maxdepth 1  -name 'auto*' -print -quit )" ) \
    || (echo no auto patch && false)
git revert --no-edit $(git log --format=%h --grep 'Commit Debian 3.0 (quilt) metadata' | head -1) 
dch --team -D unstable "Regenerate source package with quilt patches"
git add debian/changelog
git commit -m'changelog for rebuild with fixed source package'
origtargz
if [ -z "$2" ]; then
    dgit --quilt=gbp sbuild
    dgit --quilt=gbp push-source --overwrite
    branch=$(git symbolic-ref --short HEAD)
    git push --tags origin $branch
    cd "${origdir}"
fi
