This repo houses various scripts and tools used by the emacsen-team

- rebuild-with-current-dh-elpa.sh: rebuild with current dh-elpa. This
  probably needs to be generalized a bit. It would also be nice if it
  dealt better with repos needing dgit --gbp
