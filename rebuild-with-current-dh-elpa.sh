#!/bin/sh
set -e
origdir=$(pwd)
tempdir=$(mktemp -d rebuild_$1_XXXXXX)
exec &> >(tee -a $tempdir/log.txt)
cd $tempdir
apt-get source -t unstable $1
debcheckout -a $1 || git clone git@salsa.debian.org:emacsen-team/$1
cd $1
test $(dpkg-parsechangelog -S Distribution) = 'unstable' || (echo changelog not unstable && /bin/false)
grep -q 'Maintainer: Debian Emacs' debian/control || (echo not a team package? && /bin/false)
if [ -f debian/source/options ] &&  grep -q single-debian-patch debian/source/options ; then
    quiltopts=""
else
    quiltopts="--quilt=gbp"
fi

# version without epoch
oldversion=$(dpkg-parsechangelog -S Version | sed 's/^[1-9][0-9]*://')
echo oldversion=$oldversion
dch --team -D unstable "Rebuild with current dh-elpa"
git add debian/changelog
git commit -m'changelog for rebuild with current dh-elpa'
version=$(dpkg-parsechangelog -S Version | sed 's/^[1-9][0-9]*://')
echo version=$version
dgit $quiltopts build-source
(debdiff --exclude='changelog' --diffstat ../$1_$oldversion.dsc ../$1_$version.dsc  | grep '^ 0 files changed') || (echo too much diff && /bin/false)
dgit $quiltopts sbuild
dgit $quiltopts push-source --overwrite
branch=$(git symbolic-ref --short HEAD)
git push --tags origin $branch
cd "${origdir}"


