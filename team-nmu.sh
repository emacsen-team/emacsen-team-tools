#!/bin/sh
set -e
origdir=$(pwd)
tempdir=$(mktemp -d nmu_$1_XXXXXX)
cd $tempdir
apt-get source -t unstable $1
dgit clone $1
cd $1

git remote set-url vcs-git $(debcheckout -a -p $1 | cut -f2)
git fetch vcs-git

test $(dpkg-parsechangelog -S Distribution) = 'unstable'
grep -q 'Maintainer: Debian Emacs' debian/control || (echo not a team package? && /bin/false)

oldversion=$(dpkg-parsechangelog -S Version | sed 's/^[1-9][0-9]*://')
echo oldversion=$oldversion

dch --team -D unstable "Rebuild with current dh-elpa"
git add debian/changelog
git commit -m'changelog for rebuild with current dh-elpa'

version=$(dpkg-parsechangelog -S Version | sed 's/^[1-9][0-9]*://')
echo version=$version

dgit build-source
(debdiff --exclude='changelog' --diffstat ../$1_$oldversion.dsc ../$1_$version.dsc  | grep '^ 0 files changed') || (echo too much diff && /bin/false)

dgit push-source

git checkout -b mr_$version debian/$oldversion

# this next step doesn't seem quite right in general
git merge --ff-only debian/$version ||  git cherry-pick dgit/sid
git push  -o merge_request.create vcs-git mr_$version
